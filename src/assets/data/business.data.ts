export const BusinessData = [
    {
        "id": "cbm-renovations-oakville",
        "name": "CBM Renovations",
        "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/AvrOaw4rSxXFMJhcRgBGHQ/o.jpg",
        "is_closed": false,
        "url": "https://www.yelp.com/biz/cbm-renovations-oakville?adjust_creative=WVd07wnFOhV5q6DRo9yEgg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=WVd07wnFOhV5q6DRo9yEgg",
        "review_count": 2,
        "categories": [
            {
                "alias": "contractors",
                "title": "Contractors"
            }
        ],
        "rating": 1,
        "coordinates": {
            "latitude": 43.4377177,
            "longitude": -79.7736306
        },
        "transactions": [],
        "location": {
            "address1": "",
            "address2": "",
            "address3": "",
            "city": "Oakville",
            "zip_code": "L6M 5J5",
            "country": "CA",
            "state": "ON",
            "display_address": [
                "Oakville, ON L6M 5J5",
                "Canada"
            ]
        },
        "phone": "+16477857125",
        "display_phone": "+1 647-785-7125",
        "distance": 3015.995375182
    },
    {
        "id": "peeranis-flooring-mississauga",
        "name": "Peerani's Flooring",
        "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/D0Wv0Z99gcn4magvgGUhkw/o.jpg",
        "is_closed": false,
        "url": "https://www.yelp.com/biz/peeranis-flooring-mississauga?adjust_creative=WVd07wnFOhV5q6DRo9yEgg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=WVd07wnFOhV5q6DRo9yEgg",
        "review_count": 2,
        "categories": [
            {
                "alias": "flooring",
                "title": "Flooring"
            }
        ],
        "rating": 3,
        "coordinates": {
            "latitude": 43.4995727,
            "longitude": -79.608775
        },
        "transactions": [],
        "location": {
            "address1": "2359 Royal Windsor Drive",
            "address2": "Suite 29",
            "address3": "",
            "city": "Mississauga",
            "zip_code": "L5J 4S9",
            "country": "CA",
            "state": "ON",
            "display_address": [
                "2359 Royal Windsor Drive",
                "Suite 29",
                "Mississauga, ON L5J 4S9",
                "Canada"
            ]
        },
        "phone": "+19054039991",
        "display_phone": "+1 905-403-9991",
        "distance": 12222.23401842
    },
    {
        "id": "perfect-painters-mississauga",
        "name": "Perfect Painters",
        "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/vyX4QHtt0uOcgEqRAdji-A/o.jpg",
        "is_closed": false,
        "url": "https://www.yelp.com/biz/perfect-painters-mississauga?adjust_creative=WVd07wnFOhV5q6DRo9yEgg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=WVd07wnFOhV5q6DRo9yEgg",
        "review_count": 2,
        "categories": [
            {
                "alias": "painters",
                "title": "Painters"
            }
        ],
        "rating": 3.5,
        "coordinates": {
            "latitude": 43.6593223,
            "longitude": -79.6040007
        },
        "transactions": [],
        "location": {
            "address1": "2550 Matheson Boulevard E",
            "address2": "Suite 214",
            "address3": "",
            "city": "Mississauga",
            "zip_code": "L4W 4Z1",
            "country": "CA",
            "state": "ON",
            "display_address": [
                "2550 Matheson Boulevard E",
                "Suite 214",
                "Mississauga, ON L4W 4Z1",
                "Canada"
            ]
        },
        "phone": "+14164771400",
        "display_phone": "+1 416-477-1400",
        "distance": 26574.204219239997
    },
    {
        "id": "ambiance-burlington",
        "name": "Ambiance",
        "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/Z4MT4WHxMevijS99qXjU3w/o.jpg",
        "is_closed": false,
        "url": "https://www.yelp.com/biz/ambiance-burlington?adjust_creative=WVd07wnFOhV5q6DRo9yEgg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=WVd07wnFOhV5q6DRo9yEgg",
        "review_count": 1,
        "categories": [
            {
                "alias": "electricians",
                "title": "Electricians"
            }
        ],
        "rating": 5,
        "coordinates": {
            "latitude": 43.3187346,
            "longitude": -79.8067395
        },
        "transactions": [],
        "location": {
            "address1": "1185 Bellview Crescent",
            "address2": "",
            "address3": "",
            "city": "Burlington",
            "zip_code": "L7S 1C9",
            "country": "CA",
            "state": "ON",
            "display_address": [
                "1185 Bellview Crescent",
                "Burlington, ON L7S 1C9",
                "Canada"
            ]
        },
        "phone": "+19056373350",
        "display_phone": "+1 905-637-3350",
        "distance": 14679.07016516
    },
    {
        "id": "victorious-flooring-mississauga",
        "name": "Victorious Flooring",
        "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/AhAUhHzFbTSNLzH-14golg/o.jpg",
        "is_closed": false,
        "url": "https://www.yelp.com/biz/victorious-flooring-mississauga?adjust_creative=WVd07wnFOhV5q6DRo9yEgg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=WVd07wnFOhV5q6DRo9yEgg",
        "review_count": 3,
        "categories": [
            {
                "alias": "carpetinstallation",
                "title": "Carpet Installation"
            },
            {
                "alias": "flooring",
                "title": "Flooring"
            },
            {
                "alias": "carpeting",
                "title": "Carpeting"
            }
        ],
        "rating": 5,
        "coordinates": {
            "latitude": 43.6658687,
            "longitude": -79.6715947
        },
        "transactions": [],
        "location": {
            "address1": "6750 Columbus Road",
            "address2": "",
            "address3": "",
            "city": "Mississauga",
            "zip_code": "L5T 2G1",
            "country": "CA",
            "state": "ON",
            "display_address": [
                "6750 Columbus Road",
                "Mississauga, ON L5T 2G1",
                "Canada"
            ]
        },
        "phone": "+14168587006",
        "display_phone": "+1 416-858-7006",
        "distance": 25603.04477756
    },
    {
        "id": "hamilton-house-painters-hamilton-2",
        "name": "Hamilton House Painters",
        "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/jCzVW4zoFJHi6m12AAPlkQ/o.jpg",
        "is_closed": false,
        "url": "https://www.yelp.com/biz/hamilton-house-painters-hamilton-2?adjust_creative=WVd07wnFOhV5q6DRo9yEgg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=WVd07wnFOhV5q6DRo9yEgg",
        "review_count": 2,
        "categories": [
            {
                "alias": "painters",
                "title": "Painters"
            },
            {
                "alias": "flooring",
                "title": "Flooring"
            }
        ],
        "rating": 4,
        "coordinates": {
            "latitude": 43.25341,
            "longitude": -79.8789
        },
        "transactions": [],
        "location": {
            "address1": "",
            "address2": "",
            "address3": "",
            "city": "Hamilton",
            "zip_code": "L8P 1X9",
            "country": "CA",
            "state": "ON",
            "display_address": [
                "Hamilton, ON L8P 1X9",
                "Canada"
            ]
        },
        "phone": "+12898150945",
        "display_phone": "+1 289-815-0945",
        "distance": 23799.447783519998
    },
    {
        "id": "carter-electrical-mississauga-2",
        "name": "Carter Electrical",
        "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/6GzK_sVZmFu69NzOT4e5Tw/o.jpg",
        "is_closed": false,
        "url": "https://www.yelp.com/biz/carter-electrical-mississauga-2?adjust_creative=WVd07wnFOhV5q6DRo9yEgg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=WVd07wnFOhV5q6DRo9yEgg",
        "review_count": 2,
        "categories": [
            {
                "alias": "electricians",
                "title": "Electricians"
            },
            {
                "alias": "handyman",
                "title": "Handyman"
            },
            {
                "alias": "lighting",
                "title": "Lighting Fixtures & Equipment"
            }
        ],
        "rating": 5,
        "coordinates": {
            "latitude": 43.5895478689013,
            "longitude": -79.5747937129191
        },
        "transactions": [],
        "location": {
            "address1": "1609 Holbourne Road",
            "address2": null,
            "address3": "",
            "city": "Mississauga",
            "zip_code": "L5E 2L7",
            "country": "CA",
            "state": "ON",
            "display_address": [
                "1609 Holbourne Road",
                "Mississauga, ON L5E 2L7",
                "Canada"
            ]
        },
        "phone": "+14168870069",
        "display_phone": "+1 416-887-0069",
        "distance": 21089.5075163
    },
    {
        "id": "handyman-007-toronto-2",
        "name": "Handyman 007",
        "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/MH-v2-zYUA7lJOsGGH2IRA/o.jpg",
        "is_closed": false,
        "url": "https://www.yelp.com/biz/handyman-007-toronto-2?adjust_creative=WVd07wnFOhV5q6DRo9yEgg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=WVd07wnFOhV5q6DRo9yEgg",
        "review_count": 2,
        "categories": [
            {
                "alias": "handyman",
                "title": "Handyman"
            }
        ],
        "rating": 4.5,
        "coordinates": {
            "latitude": 43.7555301,
            "longitude": -79.4742274
        },
        "transactions": [],
        "location": {
            "address1": "1170 Sheppard Avenue W",
            "address2": "Unit 7",
            "address3": "",
            "city": "Toronto",
            "zip_code": "M3K 2A3",
            "country": "CA",
            "state": "ON",
            "display_address": [
                "1170 Sheppard Avenue W",
                "Unit 7",
                "Toronto, ON M3K 2A3",
                "Canada"
            ]
        },
        "phone": "+14166351638",
        "display_phone": "+1 416-635-1638",
        "distance": 40905.99168712
    },
    {
        "id": "razno-renovation-toronto",
        "name": "Razno Renovation",
        "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/PjwbO9jyZtTGUwWjiO--0Q/o.jpg",
        "is_closed": false,
        "url": "https://www.yelp.com/biz/razno-renovation-toronto?adjust_creative=WVd07wnFOhV5q6DRo9yEgg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=WVd07wnFOhV5q6DRo9yEgg",
        "review_count": 3,
        "categories": [
            {
                "alias": "flooring",
                "title": "Flooring"
            },
            {
                "alias": "drywall",
                "title": "Drywall Installation & Repair"
            }
        ],
        "rating": 3.5,
        "coordinates": {
            "latitude": 43.67358,
            "longitude": -79.4258
        },
        "transactions": [],
        "location": {
            "address1": "353 Melita Avenue",
            "address2": "",
            "address3": "",
            "city": "Toronto",
            "zip_code": "M6G 3X1",
            "country": "CA",
            "state": "ON",
            "display_address": [
                "353 Melita Avenue",
                "Toronto, ON M6G 3X1",
                "Canada"
            ]
        },
        "phone": "+14165707330",
        "display_phone": "+1 416-570-7330",
        "distance": 36034.7480334
    },
    {
        "id": "all-electricall-stoney-creek",
        "name": "All Electricall",
        "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/v4SEJinRM0USk0dD-tpwJA/o.jpg",
        "is_closed": false,
        "url": "https://www.yelp.com/biz/all-electricall-stoney-creek?adjust_creative=WVd07wnFOhV5q6DRo9yEgg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=WVd07wnFOhV5q6DRo9yEgg",
        "review_count": 2,
        "categories": [
            {
                "alias": "electricians",
                "title": "Electricians"
            }
        ],
        "rating": 3,
        "coordinates": {
            "latitude": 43.2189801,
            "longitude": -79.7670173
        },
        "transactions": [],
        "location": {
            "address1": "144 King Street W",
            "address2": "Unit 1",
            "address3": "",
            "city": "Stoney Creek",
            "zip_code": "L8G 0A9",
            "country": "CA",
            "state": "ON",
            "display_address": [
                "144 King Street W",
                "Unit 1",
                "Stoney Creek, ON L8G 0A9",
                "Canada"
            ]
        },
        "phone": "+19059308999",
        "display_phone": "+1 905-930-8999",
        "distance": 24751.636325279997
    }
]