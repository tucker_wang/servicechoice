import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { environment } from '../environments/environment';

import { AngularFireModule } from 'angularfire2';
export const firebaseConfig = environment.firebaseConfig;
import { AngularFirestoreModule } from 'angularfire2/firestore';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { ContactComponent } from './home/containers/contact/contact.component';
import { RegistrationComponent } from './home/containers/registration/registration.component';
import { SearchComponent } from './home/containers/search/search.component';
import { AboutComponent } from './home/containers/about/about.component';
import { NotfoundComponent } from './home/containers/notfound/notfound.component';
import { SearchModule } from './home/containers/search/search.module';
import { CoreModule } from './core/core.module';
import { ShareModule } from './share/share.module';
import { UserLoginComponent } from './home/containers/registration/user-login/user-login.component';
import { UserProfileComponent } from './home/containers/user-profile/user-profile.component';
import { BusinessCreditComponent } from './home/containers/user-profile/business-credit/business-credit.component';
import { UserResolveService } from './home/containers/user-profile/user-resolver.service';


@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    ContactComponent,
    RegistrationComponent,
    UserLoginComponent,
    NotfoundComponent,
    UserProfileComponent,
    BusinessCreditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ShareModule,
    CoreModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UserResolveService],
  bootstrap: [AppComponent]
})
export class AppModule { }
