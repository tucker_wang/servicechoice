import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';


import { BBBService } from '../../../api/bbb.service';
import { YelpService } from '../../../api/yelp.service';
import { SearchComponent } from './search.component';
import { ResultComponent } from './result/result.component';
import { AzureService } from '../../../api/azure.service';
import { ResultResolveService } from './result/result-resolve.service';
import { ShareModule } from '../../../share/share.module';
import { ResultDetailComponent } from './result-detail/result-detail.component';
import { CoreModule } from '../../../core/core.module';

const routes: Routes = [
    {path: '', component: SearchComponent},
    {
        path: 'result',
        resolve: {result: ResultResolveService},
        children:
        [
            {path: '', component: ResultComponent},
            {path: ':id', component: ResultDetailComponent}
        ]
    }
];

@NgModule({
    imports: [
        ShareModule,
        CommonModule,
        HttpClientModule,
        CoreModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    declarations: [SearchComponent, ResultComponent,
    ResultDetailComponent
],
    providers: [
        BBBService,
        YelpService,
        ResultResolveService
    ],
})
export class SearchModule { }
