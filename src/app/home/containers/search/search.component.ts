import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router  } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';
import { SearchTerms } from './models/searchTerms';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchForm: FormGroup;
  searchTerms: SearchTerms;
  processing: boolean = false;
  stateList: string[] = [
    'AB',
    'BC',
    'MB',
    'NB',
    'NL',
    'NU',
    'NS',
    'NT',
    'ON',
    'PE',
    'QC',
    'SK',
    'YT'
  ];

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.searchForm = this.fb.group(
      {
        term: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required],
        zip_code: ''
      }
    );
  }

  navigateToResult() {
    this.processing = true;
    setTimeout(() => {
      this.processing = false;
      const terms = Object.assign({}, this.searchTerms, this.searchForm.value);
      this.router.navigate(['/search/result'],
      {
        queryParams: {term: terms.term, city: terms.city, state: terms.state, zipCode: terms.zip_code}
      });
    }, 1500);
  }

}
