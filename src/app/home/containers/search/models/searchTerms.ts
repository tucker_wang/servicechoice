export interface SearchTerms {
    term: string;
    city: string;
    state: string;
    zip_code: string;
}
