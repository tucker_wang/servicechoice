import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { YelpSearchResult } from '../../../../api/models/yelp-search-result';
import { BusinessInfo } from '../../../../api/models/BusinessInfo';
import { Location } from "@angular/common";

@Component({
  templateUrl: './result-detail.component.html',
  styleUrls: ['./result-detail.component.css']
})
export class ResultDetailComponent implements OnInit {

  yelpSearchResult: YelpSearchResult;
  searchResults: BusinessInfo[];
  currentBusiness: BusinessInfo;
  transUnionScore: number;
  customerRatingPercent: number;
  bbbCreditRating: string;

  constructor(private router: Router, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    console.log(`result-detail-component: id: ${id}`);


    this.route.parent.data.subscribe(data => {
      this.yelpSearchResult = data['result'];
      this.searchResults = this.yelpSearchResult.Businesses;
      this.currentBusiness = this.searchResults.find(b => b.id === id);
      if (this.currentBusiness) {
        this.customerRatingPercent = this.currentBusiness.rating * 20;
        this.getCreditScore(id, this.currentBusiness.rating);
        console.log('result-detail-component.ngOninit(): ' + JSON.stringify(this.currentBusiness));
      } else {
        console.log('result-detail-component.ngOninit(): not valid, return to search page');
        this.router.navigate(['/search']);
      }
    });


  }

  backBtnClicked() {
    this.location.back();
  }

  getCreditScore(id: string, rating: number): void {
    let leng = id.length;
    let flag = leng % 2 == 0 ? 1 : -1;

    let percent = rating / 5;

    let finalScore = 65 + 10 * percent + flag * leng / 3;

    this.transUnionScore = (finalScore < 100 && finalScore > 15) ? Math.floor(finalScore) : 65;
    this.bbbCreditRating = this.getbbbRating(this.transUnionScore);
  }

  getbbbRating(creditScore: number): string {
    if (creditScore <= 20) {
      return "C";
    } else if (creditScore > 20 && creditScore <= 40) {
      return "C+";
    } else if (creditScore > 40 && creditScore <= 55 ) {
      return "B-";
    } else if (creditScore > 55 && creditScore <= 70 ) {
      return "B";
    } else if (creditScore > 70 && creditScore <= 80 ) {
      return "B+";
    } else if (creditScore > 80 && creditScore <= 85) {
      return "A-";
    } else if (creditScore > 85 && creditScore <= 95) {
      return "A";
    } else {
      return "A+"
    }
  }

}
