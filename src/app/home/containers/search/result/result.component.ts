import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BusinessInfo } from '../../../../api/models/BusinessInfo';
import { YelpService } from '../../../../api/yelp.service';
import { BusinessData } from '../../../../../assets/data/business.data';
import { YelpSearchResult } from '../../../../api/models/yelp-search-result';
import { AzureService } from '../../../../api/azure.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  searchResults: BusinessInfo[];
  yelpSearchResult: YelpSearchResult;

  total: number;
  cityName: string;
  state: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.yelpSearchResult = this.route.snapshot.data['result'];
    this.searchResults = this.yelpSearchResult.Businesses;
    this.total = this.searchResults.length;
  }
}
