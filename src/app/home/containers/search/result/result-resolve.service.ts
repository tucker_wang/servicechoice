import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';

import { YelpSearchResult } from '../../../../api/models/yelp-search-result';
import { AzureService } from '../../../../api/azure.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';



@Injectable()
export class ResultResolveService implements Resolve<YelpSearchResult> {

constructor(private azureService: AzureService,
            private router: Router) { }

    // tslint:disable-next-line:max-line-length
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<YelpSearchResult> {
        const term = route.queryParams['term'];
        const city = route.queryParams['city'] || '';
        const stateName = route.queryParams['state'] || '';
        const zipCode = route.queryParams['zipCode'] || '' ;

        return this.azureService.getYelpSearchResult(term, city, stateName, zipCode)
                                .map(result => {
                                    if (result) {
                                        return result;
                                    }
                                    console.log(`result is not found: ${term}, ${city}, ${zipCode}`);
                                    this.router.navigate(['/search']);
                                    return null;
                                })
                                .catch(
                                    error => {
                                        console.log(`Retrieval error: ${error}`);
                                        this.router.navigate(['/search']);
                                        return Observable.of(null);
                                    }
                                );
    }

}
