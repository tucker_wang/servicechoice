/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ResultResolveService } from './result-resolve.service';

describe('Service: ResultResolve', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResultResolveService]
    });
  });

  it('should ...', inject([ResultResolveService], (service: ResultResolveService) => {
    expect(service).toBeTruthy();
  }));
});