import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth.service';
import { AzureService } from '../../../api/azure.service';
import { SmallBusinessInfo } from '../../../api/models/SmallBusinessInfo';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private auth: AuthService, 
              private azureService: AzureService) { }


  ngOnInit(): void {
  }

}
