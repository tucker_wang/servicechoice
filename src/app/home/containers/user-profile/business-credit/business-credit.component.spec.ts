import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessCreditComponent } from './business-credit.component';

describe('BusinessCreditComponent', () => {
  let component: BusinessCreditComponent;
  let fixture: ComponentFixture<BusinessCreditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessCreditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
