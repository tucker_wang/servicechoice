import { Component, OnInit, Input } from '@angular/core';
import { AzureService } from '../../../../api/azure.service';
import { BusinessInfo } from '../../../../api/models/BusinessInfo';
import { SmallBusinessInfo } from '../../../../api/models/SmallBusinessInfo';

@Component({
  selector: 'app-business-credit',
  templateUrl: './business-credit.component.html',
  styleUrls: ['./business-credit.component.css']
})
export class BusinessCreditComponent implements OnInit {

  @Input() businessId;
  businessInfo: SmallBusinessInfo;
  creditScore: number;
  bbbRating: string;

  constructor(private azureClient: AzureService) { }


  ngOnInit() {
    this.azureClient.getBusinessInfo(this.businessId).subscribe(
      (businessInfo: SmallBusinessInfo) => {
        this.businessInfo = Object.assign({}, businessInfo);
        this.getCreditInfo();
      }
    ) 
  }

  getCreditInfo() {
    if (this.businessInfo) {
      this.creditScore = this.getCreditScore(this.businessInfo.id, this.businessInfo.rating);
      this.bbbRating = this.getbbbRating(this.creditScore);
    }
  }

  getbbbRating(creditScore: number): string {
    if (creditScore <= 20) {
      return "C";
    } else if (creditScore > 20 && creditScore <= 40) {
      return "C+";
    } else if (creditScore > 40 && creditScore <= 55 ) {
      return "B-";
    } else if (creditScore > 55 && creditScore <= 70 ) {
      return "B";
    } else if (creditScore > 70 && creditScore <= 80 ) {
      return "B+";
    } else if (creditScore > 80 && creditScore <= 85) {
      return "A-";
    } else if (creditScore > 85 && creditScore <= 95) {
      return "A";
    } else {
      return "A+"
    }
  }

  getCreditScore(id: string, rating: string): number {
    let leng = id.length;
    let flag = leng % 2 == 0 ? 1 : -1;

    let ratingNumber = +rating;

    let percent = ratingNumber / 5;

    let finalScore = 65 + 10 * percent + flag * leng / 3;

    return (finalScore < 100 && finalScore > 15) ? Math.floor(finalScore) : 65;
  }

}
