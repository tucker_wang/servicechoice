import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { BusinessInfo } from '../../../api/models/BusinessInfo';
import { AzureService } from '../../../api/azure.service';
import { AuthService } from '../../../core/auth.service';
import { SmallBusinessInfo } from '../../../api/models/SmallBusinessInfo';



@Injectable()
export class UserResolveService implements Resolve<SmallBusinessInfo> {

constructor(private azureService: AzureService,
            private auth: AuthService,
            private router: Router) { }

    // tslint:disable-next-line:max-line-length
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SmallBusinessInfo> {

        return this.azureService.getBusinessInfo('')
                                .map(businessInfo => {
                                    if (businessInfo) {
                                        return businessInfo;
                                    }
                                    console.log(`business is not found`);
                                    this.router.navigate(['/about']);
                                    return null;
                                })
                                .catch(
                                    error => {
                                        console.log(`Retrieval error: ${error}`);
                                        this.router.navigate(['/about']);
                                        return Observable.of(null);
                                    }
                                );
    }

}
