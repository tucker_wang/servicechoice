import { Component } from '@angular/core';
import { AuthService } from './core/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ServiceChoice';
  processing: boolean = false;

  constructor(public auth: AuthService) {} 

  signout() {
    this.processing = true;
    setTimeout(() => {
      this.auth.signOut();
      this.processing = false;
    }, 1500);
  }
}
