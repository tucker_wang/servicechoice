import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BusinessInfo } from './models/BusinessInfo';
import 'rxjs/add/operator/do';

import { environment } from '../../environments/environment';
import { YelpSearchResult } from './models/yelp-search-result';

@Injectable()
export class YelpService {

    private inMemoryUrl = 'api/businesses';
    private baseUrl = environment.YELP_API_PATH;
    private token = environment.YELP_API_TOKEN;

    constructor(private httpClient: HttpClient) { }

    getMockBusinesses(): Observable<BusinessInfo[]> {
        return this.httpClient.get<BusinessInfo[]>(this.inMemoryUrl)
            .do(data => console.log('getBusinesses: ' + JSON.stringify(data)));
    }

    getYelpSearchResult(term: string, city: string, zipcode: string): Observable<YelpSearchResult> {

        // const headers = new HttpHeaders({'authorization': 'bearer ' + this.token, 'Accept': 'application/json'});
        // headers = headers.set('authorization', 'bearer ' + this.token)
        //                 .set('Accept', 'application/json')
        //                 .set('Accept-Language', 'en-CA');

        let params = new HttpParams();

        params = params.append('term', term);
        params = params.append('location', `city=${city}&zip_code=${zipcode}`);

        return this.httpClient.get<YelpSearchResult>(`${this.baseUrl}/search`,
            {
                params: params
            }
        );


    }

}
