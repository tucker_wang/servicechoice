import { BusinessInfo } from "./businessInfo";
import { Center } from "./center";

export interface YelpSearchResult {
    Total: number;
    Businesses: BusinessInfo[];
    Region: Center;
}