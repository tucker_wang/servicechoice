import { Category } from './category';
import { Coordinate } from './coordinate';
import { Location } from './location';

export interface SmallBusinessInfo {
    id: string;
    name: string;
    image_url: string;
    is_closed: boolean;
    url: string;
    review_count: number;
    rating: string;
    phone: string;
    location: Location;
}
