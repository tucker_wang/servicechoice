import { Category } from './category';
import { Coordinate } from './coordinate';
import { Location } from './location';

export interface BusinessInfo {
    rating: number;
    phone: string;
    display_phone: string;
    id: string;
    is_closed: boolean;
    categories: Category[];
    review_count: number;
    name: string;
    url: string;
    coordinates: Coordinate;
    image_url: string;
    location: Location;
    distance: number;
    transactions: string[];
}
