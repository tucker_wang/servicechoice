import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BusinessInfo } from './models/BusinessInfo';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { environment } from '../../environments/environment';
import { YelpSearchResult } from './models/yelp-search-result';
import { SmallBusinessInfo } from './models/SmallBusinessInfo';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AzureService {

    private baseUrl = environment.AZURE_API_PATH;

    constructor(private httpClient: HttpClient) { }

    getBusinessInfo(businessId: string): Observable<SmallBusinessInfo> {
        return this.httpClient.get<SmallBusinessInfo>(`${this.baseUrl}/business/${businessId}`)
                                .pipe(
                                    catchError(this.handleError)
                                );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // return an ErrorObservable with a user-facing error message
        return new ErrorObservable(
          'Something bad happened; please try again later.');
      };

    getYelpSearchResult(term: string, city: string, state: string, zipcode: string): Observable<YelpSearchResult> {

        // const headers = new HttpHeaders({'authorization': 'bearer ' + this.token, 'Accept': 'application/json'});
        // headers = headers.set('authorization', 'bearer ' + this.token)
        //                 .set('Accept', 'application/json')
        //                 .set('Accept-Language', 'en-CA');

        let params = new HttpParams();

        params = params.append('term', term);
        params = params.append('city', city);
        params = params.append('state', state);
        params = params.append('zipCode', zipcode);

        return this.httpClient.get<YelpSearchResult>(`${this.baseUrl}/search`,
                                {
                                    params: params
                                }
                            ).do(
                                data => console.log(JSON.stringify(data))
                            );
    }

}
