import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AboutComponent } from './home/containers/about/about.component';
import { ContactComponent } from './home/containers/contact/contact.component';
import { RegistrationComponent } from './home/containers/registration/registration.component';
import { NotfoundComponent } from './home/containers/notfound/notfound.component';
import { SearchModule } from './home/containers/search/search.module';
import { UserLoginComponent } from './home/containers/registration/user-login/user-login.component';
import { UserProfileComponent } from './home/containers/user-profile/user-profile.component';
import { UserResolveService } from './home/containers/user-profile/user-resolver.service';



const routes: Routes = [
    {path: 'about', component: AboutComponent},
    {path: 'login', component: UserLoginComponent},
    {
        path: 'profile', 
        component: UserProfileComponent
    },
    {path: 'search', loadChildren: 'app/home/containers/search/search.module#SearchModule'},
    {path: 'notfound', component: NotfoundComponent},
    {path: '', redirectTo: 'about', pathMatch: 'full'},
    {path: '**', redirectTo: 'notfound', pathMatch: 'full'}
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {}
